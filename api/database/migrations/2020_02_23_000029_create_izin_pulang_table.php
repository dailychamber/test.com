<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIzinPulangTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'izin_pulang';

    /**
     * Run the migrations.
     * @table izin_pulang
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('data_id');
            $table->date('date_from');
            $table->date('date_to');
            $table->string('hari_from', 1);
            $table->string('hari_to', 1);
            $table->string('SANTRI_ID', 50);
            $table->text('keperluan');
            $table->text('keterangan');
            $table->string('thn_ajaran_id', 50);
            $table->string('created_by', 50);
            $table->string('updated_by', 50);
            $table->string('asrama_id', 50);
            $table->text('asrama');
            $table->string('kamar_id', 50);
            $table->text('kamar');
            $table->text('lembaga');
            $table->text('alamat');
            $table->string('tepat_telat', 1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
