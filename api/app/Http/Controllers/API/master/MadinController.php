<?php

namespace App\Http\Controllers\api\master;

use App\Http\Controllers\Controller;
use App\model\master\Madins;
use Illuminate\Http\Request;

class MadinController extends Controller
{
    public function index()
    {
        $data = Madins::all();
        return response()->json([
            "status" => 'ok',
            "data" => $data
        ]);
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'madin_name' => 'required',
            'order_id' => 'required',
            'status' => 'required'
        ]);

        if (!$validator->fails()) {
            // validasi sukses
            $cek = Madins::where(['madin_name' => $request->madin_name])->count();
            if ($cek == 0) {
                $madin = Madins::create($request->all());
                if ($madin) {
                    return response()->json(['status' => 'ok', 'message' => 'Data sudah tersimpan']);
                } else {
                    return response()->json(['status' => 'gagal', 'message' => 'Gagal menyimpan data.']);
                }
            } else {
                return response()->json(['status' => 'gagal', 'message' => 'Data sudah digunakan, harap isikan data yang lain.']);
            }
        } else {
            //validasi error
            return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()]);
        }
    }

    public function show($id)
    {
        $data = Madins::where('madin_id', $id)->first();
        return response()->json([
            "status" => "ok",
            "data" => $data
        ]);
    }

    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'madin_name' => 'required',
            'order_id' => 'required',
            'status' => 'required'
        ]);

        //cek validasi.
        if (!$validator->fails()) {
            $madin = Madins::where('madin_id', $id)
                ->update([
                    'madin_name' => $request->madin_name,
                    'order_id' => $request->order_id,
                    'status' => $request->status
                ]);

            if ($madin) {
                return response()->json(['status' => 'ok', 'message' => 'Data berhasil diperbarui']);
            } else {
                return response()->json(['status' => 'gagal', 'message' => 'Gagal menyimpan data.']);
            }
        } else {
            //validasi error
            return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()]);
        }
    }

    public function destroy($id)
    {
        $madin = Madins::destroy($id);
        if ($madin) {
            return response()->json(['status' => 'Data berhasil dihapus']);
        } else {
            return response()->json(['status' => 'gagal', 'message' => 'Gagal menghapus data.']);
        }
    }
}
