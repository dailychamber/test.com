<?php

namespace App\Custom;

use Illuminate\Http\Request;
use App\Model\Setting\Menu;
use App\Model\Setting\Access;
use App\User;
use Illuminate\Support\Facades\Session;

class Satpam
{ 
    public static function cekIzin($fitur,$link){
        //data user dan role
        $user = User::select('users.*','roles.access_level')
            ->join('roles', 'role_id', '=', 'roles.id')
            ->where('users.id', Session::get('user_id'))->first();
        if($user == null){
            return false;
        }

        //cek izin
        $list_fitur = Access::select('access_fitur')
            ->join('roles','role_id','=','roles.id')
            ->join('menu','menu_id','=','menu.id')
            ->where('link_menu', $link)->first();
            
        //pengecualian superuser
        if($user->access_level == 1) return true;

        if($list_fitur != null){

            //cek fitur
            $arr_list_fitur = explode('|',$list_fitur->access_fitur);
            if(in_array($fitur,$arr_list_fitur)){
                // ok lanjut
                return true;
            }else{
                // nggak punya hak akses fitur
                return false;
            }
        }else{
            // nggak ada fitur
            return false;
        }
    }
    public static function test(){
        
    }
}
