<?php

namespace App\model\master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Formals extends Model
{
    protected $table = 'formal';
    protected $primaryKey = 'formal_id';
    public $timestamps = false;
    protected $guarded = ['formal_id'];
    use SoftDeletes;
}
