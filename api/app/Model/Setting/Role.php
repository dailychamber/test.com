<?php

namespace App\Model\Setting;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Role extends Model
{
    protected $table = 'roles';
    protected $fillable = [
        'name', 'slug', 'description',
    ];

}
