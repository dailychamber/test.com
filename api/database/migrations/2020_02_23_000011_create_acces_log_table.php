<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccesLogTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'acces_log';

    /**
     * Run the migrations.
     * @table acces_log
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->string('log_id', 50)->nullable()->default(null);
            $table->dateTime('access_date')->nullable()->default(null);
            $table->string('user_id', 50)->nullable()->default(null);
            $table->string('ip_address', 50)->nullable()->default(null);
            $table->string('module', 100)->nullable()->default(null);
            $table->string('page_url', 100)->nullable()->default(null);
            $table->text('page_url_complete');
            $table->string('execution_time', 100)->nullable()->default(null);
            $table->string('user_agent', 100)->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
