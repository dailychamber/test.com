<?php

namespace App\Http\Controllers\api\master;

use App\Http\Controllers\Controller;
use App\model\master\DatiSatu;
use Illuminate\Http\Request;

class DatiSatuController extends Controller
{
    public function index()
    {
        $data = DatiSatu::all();
        return response()->json([
            "status" => 'ok',
            "data" => $data
        ]);
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'dati_satu_name' => 'required',
            'uniq_color' => 'required',
            'status' => 'required'
        ]);

        if (!$validator->fails()) {
            // validasi sukses
            $cek = DatiSatu::where(['dati_satu_name' => $request->dati_satu_name])->count();
            if ($cek == 0) {
                $dati_satu = DatiSatu::create($request->all());
                if ($dati_satu) {
                    return response()->json(['status' => 'ok', 'message' => 'Data sudah tersimpan']);
                } else {
                    return response()->json(['status' => 'gagal', 'message' => 'Gagal menyimpan data.']);
                }
            } else {
                return response()->json(['status' => 'gagal', 'message' => 'Data sudah digunakan, harap isikan data yang lain.']);
            }
        } else {
            //validasi error
            return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()]);
        }
    }

    public function show($id)
    {
        $data = DatiSatu::where('dati_satu_id', $id)->first();
        return response()->json([
            "status" => "ok",
            "data" => $data
        ]);
    }

    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'dati_satu_name' => 'required',
            'uniq_color' => 'required',
            'status' => 'required'
        ]);

        //cek validasi.
        if (!$validator->fails()) {
            $dati_satu = DatiSatu::where('dati_satu_id', $id)
                ->update([
                    'dati_satu_name' => $request->dati_satu_name,
                    'uniq_color' => $request->uniq_color,
                    'status' => $request->status
                ]);

            if ($dati_satu) {
                return response()->json(['status' => 'ok', 'message' => 'Data berhasil diperbarui']);
            } else {
                return response()->json(['status' => 'gagal', 'message' => 'Gagal menyimpan data.']);
            }
        } else {
            //validasi error
            return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()]);
        }
    }

    public function destroy($id)
    {
        $dati_satu = DatiSatu::destroy($id);
        if ($dati_satu) {
            return response()->json(['status' => 'Data berhasil dihapus']);
        } else {
            return response()->json(['status' => 'gagal', 'message' => 'Gagal menghapus data.']);
        }
    }
}
