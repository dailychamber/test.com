<?php

namespace App\model\master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lpqs extends Model
{
    protected $table = 'lpq';
    protected $primaryKey = 'lpq_id';
    public $timestamps = false;
    protected $guarded = ['lpq_id'];
    use SoftDeletes;
}
