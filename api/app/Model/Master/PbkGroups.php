<?php

namespace App\model\master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PbkGroups extends Model
{
    protected $table = 'id';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $guarded = ['id'];
    use SoftDeletes;
}
