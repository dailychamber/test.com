<?php

namespace App\model\master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Psg extends Model
{
    protected $table = 'psg';
    protected $primaryKey = 'psg_id';
    public $timestamps = false;
    protected $guarded = ['psg_id'];
    use SoftDeletes;
}
