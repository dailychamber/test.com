<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtraKuriTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'extra_kuri';

    /**
     * Run the migrations.
     * @table extra_kuri
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('extra_kur_id');
            $table->string('extra_kur_name', 225);
            $table->string('status', 1);

            $table->index(["extra_kur_name"], 'extra_kur_name_2');

            $table->unique(["extra_kur_name"], 'extra_kur_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
