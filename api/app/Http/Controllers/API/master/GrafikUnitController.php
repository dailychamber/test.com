<?php

namespace App\Http\Controllers\api\master;

use App\Http\Controllers\Controller;
use App\model\master\GrafikUnits;
use Illuminate\Http\Request;

class GrafikUnitController extends Controller
{

    public function index()
    {
        $data = GrafikUnits::all();
        return response()->json([
            "status" => 'ok',
            "data" => $data
        ]);
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'kelas' => 'required',
            'jml' => 'required',
            'thn_ajaran_id' => 'required',
            'kategori_gen' => 'required',
            'data_bulan' => 'required'
        ]);

        if (!$validator->fails()) {
            // validasi sukses
            $cek = GrafikUnits::where(['kelas' => $request->kelas, 'thn_ajaran_id' => $request->thn_ajaran_id])->count();
            if ($cek == 0) {
                $grafik_unit = GrafikUnits::create($request->all());
                if ($grafik_unit) {
                    return response()->json(['status' => 'ok', 'message' => 'Data sudah tersimpan']);
                } else {
                    return response()->json(['status' => 'gagal', 'message' => 'Gagal menyimpan data.']);
                }
            } else {
                return response()->json(['status' => 'gagal', 'message' => 'Data sudah digunakan, harap isikan data yang lain.']);
            }
        } else {
            //validasi error
            return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()]);
        }
    }

    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'kelas' => 'required',
            'jml' => 'required',
            'thn_ajaran_id' => 'required',
            'kategori_gen' => 'required',
            'data_bulan' => 'required'
        ]);

        //cek validasi.
        if (!$validator->fails()) {
            $grafik_unit = GrafikUnits::where('formal', $id)
                ->update([
                    'kelas' => $request->kelas,
                    'jml' => $request->jml,
                    'thn_ajaran_id' => $request->thn_ajaran_id,
                    'kategori_gen' => $request->kategori_gen,
                    'data_bulan' => $request->data_bulan,
                ]);

            if ($grafik_unit) {
                return response()->json(['status' => 'ok', 'message' => 'Data berhasil diperbarui']);
            } else {
                return response()->json(['status' => 'gagal', 'message' => 'Gagal menyimpan data.']);
            }
        } else {
            //validasi error
            return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()]);
        }
    }

    public function destroy($id)
    {
        $grafik_unit = GrafikUnits::destroy($id);
        if ($grafik_unit) {
            return response()->json(['status' => 'Data berhasil dihapus']);
        } else {
            return response()->json(['status' => 'gagal', 'message' => 'Gagal menghapus data.']);
        }
    }
}
