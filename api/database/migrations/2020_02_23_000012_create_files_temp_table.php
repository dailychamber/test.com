<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTempTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'files_temp';

    /**
     * Run the migrations.
     * @table files_temp
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('id');
            $table->string('file_name', 200);
            $table->string('file_path');
            $table->string('base_path');
            $table->string('file_type', 20)->comment('by extensions');
            $table->string('file_myme', 100);
            $table->double('file_size');
            $table->string('original_name', 200);
            $table->string('create_by', 50);
            $table->dateTime('create_at');
            $table->smallInteger('uploaded')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
