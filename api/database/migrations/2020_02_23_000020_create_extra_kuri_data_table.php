<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtraKuriDataTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'extra_kuri_data';

    /**
     * Run the migrations.
     * @table extra_kuri_data
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('extra_kuri_data_id');
            $table->string('extra_kur_id', 50);
            $table->string('SANTRI_ID', 50);
            $table->string('thn_ajaran_id', 50);
            $table->string('kamar_id', 50);
            $table->string('asrama_id', 50);
            $table->string('status', 1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
