<?php

namespace App\Http\Controllers\API\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Model\Setting\Role;
use App\Model\Setting\Menu;
use App\Custom\Satpam;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['role'] = Role::whereNull('deleted_at')->get();
        $data['menu'] = Menu::whereNull('deleted_at')->get();
        return response()->json(
            [
                "status" => "ok",
                "data" => $data
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = $request->name;
        $slug = $request->slug;
        $description = $request->description;

        $role = new Role;
        $role->name = $name;
        $role->slug = $slug;
        $role->description = $description;
        $role->save();
        
        for ($i=0; $i < count($request->menu); $i++) { 
            DB::table("access")->insert([
                "role_id" => $role->id,
                "menu_id" => $request->menu[$i]
            ]);
        }
        

        return response()->json(['status' => 'ok']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Role::where('id',$id)->first();
        $menu = DB::table("access")->where("role_id", $id)->get();
        return response()->json([
            "data" => $data,
            "menu" => $menu
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = $request->name;
        $slug = $request->slug;
        $description = $request->description;

        $role = Role::find($id);
        $role->name = $name;
        $role->slug = $slug;
        $role->description = $description;
        $role->save();

        if (is_array($request->menu)) {
            # code...
            $count = DB::table("access")->where("role_id", $id)->count();
            if ($count != count($request->menu)) {
                DB::table("access")->where("role_id", $id)->delete();
                for ($i=0; $i < count($request->menu); $i++) { 
                    DB::table("access")->insert([
                        "role_id" => $role->id,
                        "menu_id" => $request->menu[$i]
                    ]);
                }
            }
        }else {
            DB::table("access")->where("role_id", $id)->delete();
        }

        return response()->json(['status' => 'ok']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table("access")->where("role_id", $id)->delete();
        $role = Role::find($id);
        $role->delete();
        return response()->json(['status' => 'ok']);
    }
}
