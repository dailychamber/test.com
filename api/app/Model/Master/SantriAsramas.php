<?php

namespace App\model\master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SantriAsramas extends Model
{
    protected $table = 'santri_asrama';
    protected $primaryKey = 'asrama_id';
    public $timestamps = false;
    protected $guarded = ['asrama_id'];
    use SoftDeletes;
}
