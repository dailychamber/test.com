<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateThnAjaranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('thn_ajaran', function (Blueprint $table) {
            $table->bigIncrements('thn_ajaran_id');
            $table->string('tahun');
            $table->string('tahun_ajaran_name');
            $table->date('date_from');
            $table->date('date_to');
            $table->char('status');
            $table->integer('is_now');
            $table->text('date_serialized');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('thn_ajaran');
    }
}
