<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePsgTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'psg';

    /**
     * Run the migrations.
     * @table psg
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('PSG_ID');
            $table->string('SANTRI_ID', 50);
            $table->string('santri_asrama', 50);
            $table->string('KAMAR_ID', 50);
            $table->string('thn_ajaran_id', 50);
            $table->date('date_from');
            $table->date('date_to');
            $table->text('durasi_serialized');
            $table->text('keterangan');
            $table->string('status', 1);
            $table->dateTime('created_at');
            $table->string('created_by', 50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
