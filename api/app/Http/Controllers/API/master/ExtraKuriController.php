<?php

namespace App\Http\Controllers\API\master;

use App\Http\Controllers\Controller;
use App\Model\master\ExtraKuris;
use Illuminate\Http\Request;

class ExtraKuriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ExtraKuris::all();
        return response()->json([
            "status" => 'ok',
            "data" => $data
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'extra_kur_name' => 'required',
            'status' => 'required'
        ]);

        if (!$validator->fails()) {
            // validasi sukses
            $cek = ExtraKuris::where(['extra_kur_id' => $request->extra_kur_id])->count();
            if ($cek == 0) {
                $extra_kuri = ExtraKuris::create($request->all());
                if ($extra_kuri) {
                    return response()->json(['status' => 'ok', 'message' => 'Data sudah tersimpan']);
                } else {
                    return response()->json(['status' => 'gagal', 'message' => 'Gagal menyimpan data.']);
                }
            } else {
                return response()->json(['status' => 'gagal', 'message' => 'Data sudah digunakan, harap isikan data yang lain.']);
            }
        } else {
            //validasi error
            return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\master\ExtraKuris  $extraKuris
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ExtraKuris::where('extra_kur_id', $id)->first();
        return response()->json([
            "status" => "ok",
            "data" => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\master\ExtraKuris  $extraKuris
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'extra_kur_name' => 'required',
            'status' => 'required'
        ]);

        //cek validasi.
        if (!$validator->fails()) {
            $extra_kuri = ExtraKuris::where('extra_kur_id', $id)
                ->update([
                    'extra_kur_name' => $request->extra_kur_name,
                    'status' => $request->status
                ]);

            if ($extra_kuri) {
                return response()->json(['status' => 'ok', 'message' => 'Data berhasil diperbarui']);
            } else {
                return response()->json(['status' => 'gagal', 'message' => 'Gagal menyimpan data.']);
            }
        } else {
            //validasi error
            return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()]);
        }
    }

    public function destroy($id)
    {
        $extra_kuri = ExtraKuris::destroy($id);
        if ($extra_kuri) {
            return response()->json(['status' => 'Data berhasil dihapus']);
        } else {
            return response()->json(['status' => 'gagal', 'message' => 'Gagal menghapus data.']);
        }
    }
}
