<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMadinUnitTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'madin_unit';

    /**
     * Run the migrations.
     * @table madin_unit
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('SANTRI_ID');
            $table->string('MADIN_UNIT', 50);
            $table->string('MADIN_KELAS', 50);
            $table->string('MADIN_TINGKAT', 50);
            $table->string('SDR_STATUS', 1);
            $table->string('ASRAMA', 50);
            $table->string('KAMAR_ID', 50);
            $table->string('thn_ajaran_id', 50);
            $table->string('data_bulan', 50);
            $table->string('STATUS', 1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
