<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJenisBiayaSyahriahTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'jenis_biaya_syahriah';

    /**
     * Run the migrations.
     * @table jenis_biaya_syahriah
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('jenis_biaya_id');
            $table->string('jenis_biaya_name', 200);
            $table->string('jenis_biaya_besar', 50);
            $table->string('thn_ajaran_id', 50);
            $table->integer('tahun');
            $table->integer('bulan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
