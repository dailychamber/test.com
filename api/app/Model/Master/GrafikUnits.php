<?php

namespace App\model\master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GrafikUnits extends Model
{
    protected $table = 'grafik_unit';
    protected $primaryKey = 'formal';
    public $timestamps = false;
    protected $guarded = ['formal'];
    use SoftDeletes;
}
