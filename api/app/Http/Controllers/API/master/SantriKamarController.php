<?php

namespace App\Http\Controllers\api\master;

use App\Http\Controllers\Controller;
use App\model\master\SantriKamars;
use Illuminate\Http\Request;

class SantriKamarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\model\master\SantriKamars  $santriKamars
     * @return \Illuminate\Http\Response
     */
    public function show(SantriKamars $santriKamars)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\model\master\SantriKamars  $santriKamars
     * @return \Illuminate\Http\Response
     */
    public function edit(SantriKamars $santriKamars)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\model\master\SantriKamars  $santriKamars
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SantriKamars $santriKamars)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\model\master\SantriKamars  $santriKamars
     * @return \Illuminate\Http\Response
     */
    public function destroy(SantriKamars $santriKamars)
    {
        //
    }
}
