<?php

namespace App\Http\Controllers\API\Master;

use App\Http\Controllers\Controller;
use App\Model\Master\ThnAjarans;
use Illuminate\Http\Request;

class ThnAjaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response 
     */
    public function index()
    {
        $data = ThnAjarans::all();
        return response()->json([
            "status" => 'ok',
            "data" => $data
        ]);
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'tahun' => 'required',
            'tahun_ajaran_name' => 'required',
            'date_from' => 'required',
            'date_to' => 'required',
            'status' => 'required',
            'is_now' => 'required',
        ]);

        //cek validasi.
        if (!$validator->fails()) {
            // validasi sukses
            $cek = ThnAjarans::where(['thn_ajaran_id' => $request->thn_ajaran_id])->count();
            if ($cek == 0) {
                $thn_ajaran = new ThnAjarans;
                $thn_ajaran->thn_ajaran_id = time();
                $thn_ajaran->tahun = $request->tahun;
                $thn_ajaran->tahun_ajaran_name = $request->tahun_ajaran_name;
                $thn_ajaran->date_from = $request->date_from;
                $thn_ajaran->date_to = $request->date_to;
                $thn_ajaran->status = $request->status;
                $thn_ajaran->is_now = $request->is_now;
                //thn_ajaran->date_serialized = ;
                //$thn_ajaran->status = 'Aktif';
                if ($thn_ajaran->save()) {
                    return response()->json(['status' => 'ok']);
                } else {
                    return response()->json(['status' => 'gagal', 'message' => 'Gagal menyimpan data.']);
                }
            } else {
                return response()->json(['status' => 'gagal', 'message' => 'Data sudah digunakan, harap isikan data yang lain.']);
            }
        } else {
            //validasi error
            return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Master\ThnAjarans  $thnAjarans
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ThnAjarans::where('thn_ajaran_id', $id)->first();
        return response()->json([
            "status" => "ok",
            "data" => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Master\ThnAjarans  $thnAjarans
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Master\ThnAjarans  $thnAjarans
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'tahun' => 'required',
            'tahun_ajaran_name' => 'required',
            'date_from' => 'required',
            'date_to' => 'required',
            'status' => 'required',
            'is_now' => 'required'
        ]);

        //cek validasi.
        if (!$validator->fails()) {
            // validasi sukses
            $thn_ajaran = ThnAjarans::find($id);
            $thn_ajaran->thn_ajaran_id = time();
            $thn_ajaran->tahun = $request->tahun;
            $thn_ajaran->tahun_ajaran_name = $request->tahun_ajaran_name;
            $thn_ajaran->date_from = $request->date_from;
            $thn_ajaran->date_to = $request->date_to;
            $thn_ajaran->status = $request->status;
            $thn_ajaran->is_now = $request->is_now;
            if ($thn_ajaran->save()) {
                return response()->json(['status' => 'ok']);
            } else {
                return response()->json(['status' => 'gagal', 'message' => 'Gagal menyimpan data.']);
            }
        } else {
            //validasi error
            return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Master\ThnAjarans  $thnAjarans
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $thn_ajaran = ThnAjarans::find($id);
        $thn_ajaran->deleted_at = date('Y-m-d H:i:s');
        if ($thn_ajaran->save()) {
            return response()->json(['status' => 'ok']);
        } else {
            return response()->json(['status' => 'gagal', 'message' => 'Gagal menghapus data.']);
        }
    }
}
