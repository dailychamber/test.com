<?php

namespace App\Http\Controllers\API\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class permissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table("access")
                              ->join("roles", "roles.id", "=", "access.role_id")
                              ->join("menu", "menu.id", "=", "access.menu_id")
                              ->select("access.*", "roles.name", "menu.nama_menu", "menu.fitur_menu", "menu.pengunci")
                            //   ->whereNull('access.deleted_at')
                              ->get();

        return response()->json([
            "status" => 'ok',
            "data" => $data
        ]);                      
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($role_id, $menu_id)
    {
        $data = DB::table("access")->where(['role_id' => $role_id, 'menu_id' => $menu_id])->first();
        return response()->json([
            "data" => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $role_id, $menu_id)
    {
        if (is_array($request->fitur_menu)) {
            $fitur_menu = implode("|",$request->fitur_menu);
        } else {
            $fitur_menu = '';
        }

        if (is_array($request->pengunci)) {
            $pengunci = implode("|",$request->pengunci);
        } else {
            $pengunci = '';
        }
        
        DB::table("access")->where(['role_id' => $role_id, 'menu_id' => $menu_id])
        ->update([
            "access_fitur" => $fitur_menu,
            "access_pengunci" => $pengunci
        ]);

        return response()->json(['status' => 'ok']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
