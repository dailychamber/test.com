<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'API\RegisterController@register')->middleware('cors');
Route::post('login', 'API\RegisterController@login')->middleware('cors');


Route::group(['middleware' => ['auth:api']], function () {

    //setting/menu
    Route::namespace('API\Setting')->prefix('setting/menu')->group(function () {
        //akses page biasa
        // Route::group(['middleware' => 'status'], function () {
        Route::get('/', 'MenuController@index');

        Route::post('/store', 'MenuController@store');
        Route::get('/edit/{id}', 'MenuController@edit');
        Route::post('/update/{id}', 'MenuController@update');
        // });
        //akses json/ajax
        // Route::group(['middleware' => 'statusGet'], function () {
        Route::post('/delete', 'MenuController@destroy');
        // });

        //setting/menu
        Route::namespace('API\Setting')->prefix('setting/menu')->group(function () {
            //akses page biasa
            // Route::group(['middleware' => 'status'], function () {
            Route::get('/', 'MenuController@index');

            Route::post('/store', 'MenuController@store');
            Route::get('/edit/{id}', 'MenuController@edit');
            Route::post('/update/{id}', 'MenuController@update');
            // });
            //akses json/ajax
            // Route::group(['middleware' => 'statusGet'], function () {
            Route::post('/delete', 'MenuController@destroy');
            // });

        });

        //setting/role
        Route::namespace('API\Setting')->prefix('setting/role')->group(function () {
            //akses page biasa
            // Route::group(['middleware' => 'status'], function () {

            Route::get('/', 'RoleController@index');

            Route::post('/store', 'RoleController@store');
            Route::get('/edit/{id}', 'RoleController@edit');
            Route::post('/update/{id}', 'RoleController@update');
            // });
            //akses json/ajax
            // Route::group(['middleware' => 'statusGet'], function () {
            Route::get('/delete/{id}', 'RoleController@destroy');

            Route::get('/', 'RoleController@index');

            Route::post('/store', 'RoleController@store');
            Route::get('/edit/{id}', 'RoleController@edit');
            Route::post('/update/{id}', 'RoleController@update');
            // });
            //akses json/ajax
            // Route::group(['middleware' => 'statusGet'], function () {
            Route::get('/delete/{id}', 'RoleController@destroy');
        });

        //setting/permission
        Route::namespace('API\Setting')->prefix('setting/permission')->group(function () {
            //akses page biasa
            // Route::group(['middleware' => 'status'], function () {

            Route::get('/', 'permissionController@index');

            Route::get('/edit/{role_id}/{menu_id}', 'permissionController@show');
            Route::post('/update/{role_id}/{menu_id}', 'permissionController@update');
            // });

            Route::get('/', 'permissionController@index');

            Route::get('/edit/{role_id}/{menu_id}', 'permissionController@show');
            Route::post('/update/{role_id}/{menu_id}', 'permissionController@update');
            // });


        });

        //setting/users
        Route::namespace('API\Setting')->prefix('setting/users')->group(function () {
            //akses page biasa
            // Route::group(['middleware' => 'status'], function () {

            Route::get('/', 'usersController@index');

            Route::post('/store', 'usersController@store');
            Route::get('/edit/{id}', 'usersController@show');
            Route::post('/update/{id}', 'usersController@update');
            Route::get('/delete/{id}', 'usersController@destroy');
            // });

            Route::get('/', 'usersController@index');

            Route::post('/store', 'usersController@store');
            Route::get('/edit/{id}', 'usersController@show');
            Route::post('/update/{id}', 'usersController@update');
            Route::get('/delete/{id}', 'usersController@destroy');
            // });


        });

        //master/unitformal
        Route::namespace('API\master')->prefix('master/unitformal')->group(function () {
            //akses page biasa
            // Route::group(['middleware' => 'status'], function () {
            Route::get('/', 'UnitFormalController@index');
            Route::post('/store', 'UnitFormalController@store');
            Route::get('/detail/{id}', 'UnitFormalController@show');
            Route::put('/update/{id}', 'UnitFormalController@update');
            Route::get('/delete/{id}', 'UnitFormalController@destroy');
        });

        Route::namespace('API\master')->prefix('master/thnajaran')->group(function () {
            //akses page biasa
            // Route::group(['middleware' => 'status'], function () {
            Route::get('/', 'ThnAjaranController@index');
            Route::post('/store', 'ThnAjaranController@store');
            Route::get('/detail/{id}', 'ThnAjaranController@show');
            Route::post('/edit/{id}', 'ThnAjaranController@update');
            Route::get('/delete/{id}', 'ThnAjaranController@destroy');
        });

        Route::namespace('API\master')->prefix('master/extrakuri')->group(function () {
            //akses page biasa
            // Route::group(['middleware' => 'status'], function () {
            Route::get('/', 'ExtraKuriController@index');
            Route::post('/store', 'ExtraKuriController@store');
            Route::get('/detail/{id}', 'ExtraKuriController@show');
            Route::post('/edit/{id}', 'ExtraKuriController@update');
            Route::get('/delete/{id}', 'ExtraKuriController@destroy');
        });

        Route::namespace('API\master')->prefix('master/formal')->group(function () {
            Route::get('/', 'FormalController@index');
            Route::post('/store', 'FormalController@store');
            Route::get('/detail/{id}', 'FormalController@show');
            Route::post('/edit/{id}', 'FormalController@update');
            Route::get('/delete/{id}', 'FormalController@destroy');
        });

        Route::namespace('API\master')->prefix('master/formalkelas')->group(function () {
            Route::get('/', 'FormalKelasController@index');
            Route::post('/store', 'FormalKelasController@store');
            Route::get('/detail/{id}', 'FormalKelasController@show');
            Route::post('/edit/{id}', 'FormalKelasController@update');
            Route::get('/delete/{id}', 'FormalKelasController@destroy');
        });

        Route::namespace('API\master')->prefix('master/grafikunit')->group(function () {
            Route::get('/', 'GrafikUnitController@index');
            Route::post('/store', 'GrafikUnitController@store');
            Route::get('/detail/{id}', 'GrafikUnitController@show');
            Route::post('/edit/{id}', 'GrafikUnitController@update');
            Route::get('/delete/{id}', 'GrafikUnitController@destroy');
        });

        Route::namespace('API\master')->prefix('master/kegiatan')->group(function () {
            Route::get('/', 'KegiatanController@index');
            Route::post('/store', 'KegiatanController@store');
            Route::get('/detail/{id}', 'KegiatanController@show');
            Route::post('/edit/{id}', 'KegiatanController@update');
            Route::get('/delete/{id}', 'KegiatanController@destroy');
        });

        Route::namespace('API\master')->prefix('master/lba')->group(function () {
            Route::get('/', 'LbaController@index');
            Route::post('/store', 'LbaController@store');
            Route::get('/detail/{id}', 'LbaController@show');
            Route::post('/edit/{id}', 'LbaController@update');
            Route::get('/delete/{id}', 'LbaController@destroy');
        });

        Route::namespace('API\master')->prefix('master/lpq')->group(function () {
            Route::get('/', 'LpqController@index');
            Route::post('/store', 'LpqController@store');
            Route::get('/detail/{id}', 'LpqController@show');
            Route::post('/edit/{id}', 'LpqController@update');
            Route::get('/delete/{id}', 'LpqController@destroy');
        });

        Route::namespace('API\master')->prefix('master/madin')->group(function () {
            Route::get('/', 'MadinController@index');
            Route::post('/store', 'MadinController@store');
            Route::get('/detail/{id}', 'MadinController@show');
            Route::post('/edit/{id}', 'MadinController@update');
            Route::get('/delete/{id}', 'MadinController@destroy');
        });

        Route::namespace('API\master')->prefix('master/madinkelas')->group(function () {
            Route::get('/', 'MadinKelasController@index');
            Route::post('/store', 'MadinKelasController@store');
            Route::get('/detail/{id}', 'MadinKelasController@show');
            Route::post('/edit/{id}', 'MadinKelasController@update');
            Route::get('/delete/{id}', 'MadinKelasController@destroy');
        });

        Route::namespace('API\master')->prefix('master/pbkgroup')->group(function () {
            Route::get('/', 'PbkGroupController@index');
            Route::post('/store', 'PbkGroupController@store');
            Route::get('/detail/{id}', 'PbkGroupController@show');
            Route::post('/edit/{id}', 'PbkGroupController@update');
            Route::get('/delete/{id}', 'PbkGroupController@destroy');
        });

        Route::namespace('API\master')->prefix('master/psg')->group(function () {
            Route::get('/', 'PsgController@index');
            Route::post('/store', 'PsgController@store');
            Route::get('/detail/{id}', 'PsgController@show');
            Route::post('/edit/{id}', 'PsgController@update');
            Route::get('/delete/{id}', 'PsgController@destroy');
        });

        Route::namespace('API\master')->prefix('master/psgbulan')->group(function () {
            Route::get('/', 'PsgBulanController@index');
            Route::post('/store', 'PsgBulanController@store');
            Route::get('/detail/{id}', 'PsgBulanController@show');
            Route::post('/edit/{id}', 'PsgBulanController@update');
            Route::get('/delete/{id}', 'PsgBulanController@destroy');
        });

        Route::namespace('API\master')->prefix('master/santriasrama')->group(function () {
            Route::get('/', 'SantriAsramaController@index');
            Route::post('/store', 'SantriAsramaController@store');
            Route::get('/detail/{id}', 'SantriAsramaController@show');
            Route::post('/edit/{id}', 'SantriAsramaController@update');
            Route::get('/delete/{id}', 'SantriAsramaController@destroy');
        });

        Route::namespace('API\master')->prefix('master/datisatu')->group(function () {
            Route::get('/', 'DatiSatuController@index');
            Route::post('/store', 'DatiSatuController@store');
            Route::get('/detail/{id}', 'DatiSatuController@show');
            Route::post('/edit/{id}', 'DatiSatuController@update');
            Route::get('/delete/{id}', 'DatiSatuController@destroy');
        });
    });
});
