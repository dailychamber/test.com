<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSantriAbsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'santri_abs';

    /**
     * Run the migrations.
     * @table santri_abs
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('SANTRI_ID');
            $table->string('kode_abs', 2);
            $table->string('santri_asrama', 50);
            $table->string('KAMAR_ID', 50);
            $table->string('thn_ajaran_id', 50);
            $table->integer('bulan');
            $table->string('tahun', 4);
            $table->date('tgl');
            $table->string('kategori_abs', 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
