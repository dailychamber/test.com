<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccessTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'access';

    /**
     * Run the migrations.
     * @table access
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('role_id');
            $table->integer('menu_id');
            $table->string('access_fitur')->nullable()->default(null);
            $table->string('access_pengunci')->nullable()->default(null);
            $table->softDeletes('deleted_at')->nullable();
            $table->timestamps();

            // $table->index(["menu_id"], 'menu_id');


            // $table->foreign('role_id', 'access_role_id')
            //     ->references('id')->on('roles')
            //     ->onDelete('restrict')
            //     ->onUpdate('restrict');

            // $table->foreign('menu_id', 'menu_id')
            //     ->references('id')->on('menu')
            //     ->onDelete('restrict')
            //     ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
