<?php

namespace App\Http\Controllers\api\master;

use App\Http\Controllers\Controller;
use App\model\master\Formalkelass;
use Illuminate\Http\Request;

class FormalKelasController extends Controller
{

    public function index()
    {
        $data = Formalkelass::all();
        return response()->json([
            "status" => 'ok',
            "data" => $data
        ]);
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'formal_id' => 'required',
            'formal_kelas_name' => 'required',
            'order_id' => 'required',
            'status' => 'required'
        ]);

        if (!$validator->fails()) {
            // validasi sukses
            $cek = Formalkelass::where(['formal_id' => $request->formal_id])->count();
            if ($cek == 0) {
                $Formalkelass = Formalkelass::create($request->all());
                if ($Formalkelass) {
                    return response()->json(['status' => 'ok', 'message' => 'Data sudah tersimpan']);
                } else {
                    return response()->json(['status' => 'gagal', 'message' => 'Gagal menyimpan data.']);
                }
            } else {
                return response()->json(['status' => 'gagal', 'message' => 'Data sudah digunakan, harap isikan data yang lain.']);
            }
        } else {
            //validasi error
            return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()]);
        }
    }

    public function show($id)
    {
        $data = Formalkelass::where('formal_kelas_id', $id)->first();
        return response()->json([
            "status" => "ok",
            "data" => $data
        ]);
    }

    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'formal_id' => 'required',
            'formal_kelas_name' => 'required',
            'order_id' => 'required',
            'status' => 'required'
        ]);

        //cek validasi.
        if (!$validator->fails()) {
            $formalkelas = Formalkelass::where('formal_kelas_id', $id)
                ->update([
                    'formal_id' => $request->formal_id,
                    'formal_kelas_name' => $request->formal_kelas_name,
                    'order_id' => $request->order_id,
                    'status' => $request->status
                ]);

            if ($formalkelas) {
                return response()->json(['status' => 'ok', 'message' => 'Data berhasil diperbarui']);
            } else {
                return response()->json(['status' => 'gagal', 'message' => 'Gagal menyimpan data.']);
            }
        } else {
            //validasi error
            return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()]);
        }
    }

    public function destroy($id)
    {
        $formalkelas = Formalkelass::destroy($id);
        if ($formalkelas) {
            return response()->json(['status' => 'Data berhasil dihapus']);
        } else {
            return response()->json(['status' => 'gagal', 'message' => 'Gagal menghapus data.']);
        }
    }
}
