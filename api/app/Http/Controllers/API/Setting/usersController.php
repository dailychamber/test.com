<?php

namespace App\Http\Controllers\API\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Model\profile;

class usersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::select('*')
        ->join('profile', 'users.profile_id', '=', 'profile.id')
        ->whereNull('users.deleted_at')->get();
        return response()->json(
            [
                "status" => "ok",
                "users" => $users
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validasi.
        $validator = \Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'min:8|required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'min:8',
            'noktp' => 'required',
            'nama_lengkap' => 'required',
            'nohp' => 'required',
        ]);

        //cek validasi.
        if (!$validator->fails()) {
            // validasi sukses
            $profile = new profile;
            $profile->noktp = $request->noktp;
            $profile->nama_lengkap = $request->nama_lengkap;
            $profile->alamat = $request->alamat;
            $profile->nohp = $request->nohp;
            $profile->nohpkuasa = $request->nohpkuasa;
            $profile->save();

            $user = new User;
            $user->profile_id = $profile->id;
            $user->name = $request->nama_lengkap;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->deleted_at = null;


            if ($user->save()) {
                return response()->json(['status' => 'ok']);
            } else {
                return response()->json(['status' => 'error', 'message' => 'Gagal menyimpan data.']);
            }

        }else{
            //validasi error
            return response()->json(['status' => 'validation_error','errors' => $validator->errors()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = User::select('*')
        ->join('profile', 'users.profile_id', '=', 'profile.id')
        ->where('profile_id', $id)
        ->whereNull('users.deleted_at')->get();
        return response()->json(
            [
                "status" => "ok",
                "users" => $users[0]
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validasi.
        $validator = \Validator::make($request->all(), [
            'nama_lengkap' => 'required',
            'nohp' => 'required',
            'foto' => 'file|max:1024', 
        ]);

        //cek validasi.
        if (!$validator->fails()) {
            if ($request->hasFile('foto')) {
                $user = Auth::user();
                $foto = $request->file('foto');
                $path = Storage::putFileAs(
                    'images/users', $request->file('foto'), $user->profile_id.".".$foto->getClientOriginalExtension()
                );
            }

            // validasi sukses
            $profile = profile::find($id);
            $profile->nama_lengkap = $request->nama_lengkap;
            $profile->alamat = $request->alamat;
            $profile->nohp = $request->nohp;
            $profile->nohpkuasa = $request->nohpkuasa;
            $profile->save();

            $user = User::where("profile_id", $id)->update([
                "name" => $request->nama_lengkap
            ]);

            if ($user) {
                return response()->json(['status' => 'ok']);
            } else {
                return response()->json(['status' => 'error', 'message' => 'Gagal menyimpan data.']);
            }

        }else{
            //validasi error
            return response()->json(['status' => 'validation_error','errors' => $validator->errors()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
