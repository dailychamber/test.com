<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([[
            'id'=>'1',
            'name'=>'Super Administrator',
            'slug'=>'superadmin',
            'description'=>'Super Administrator',
            'access_level'=>'1'
        ]]);
    }
}
