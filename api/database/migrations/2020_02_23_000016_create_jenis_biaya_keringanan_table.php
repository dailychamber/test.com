<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJenisBiayaKeringananTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'jenis_biaya_keringanan';

    /**
     * Run the migrations.
     * @table jenis_biaya_keringanan
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('jenis_biaya_id');
            $table->string('jenis_biaya_name', 225);
            $table->string('jenis_biaya_besar', 50);
            $table->string('thn_ajaran_id', 50);
            $table->string('SANTRI_ID', 50);
            $table->string('ASRAMA', 50);
            $table->text('bulan_serialized');
            $table->string('besartot_kalibulan', 50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
