<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('noktp',20);
            $table->string('nama_lengkap',255);
            $table->text('alamat')->nullable();
            $table->string('nohp', 15)->nullable();
            $table->string('nohpkuasa', 15)->nullable();
            $table->string('foto', 100)->nullable();
            $table->softDeletes('deleted_at')->nullable();
            $table->timestamps();

        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile');
    }
}
