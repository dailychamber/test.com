<?php

namespace App\Http\Controllers\API\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Setting\Menu;
use App\Custom\Satpam;

class MenuController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //cek izin (fitur) access, kalau nggak punya, tampilkan halaman 401
        // if(!Satpam::cekIzin('read',$this->link_menu)) return response()->view('errors.401');      
        $data['option'] = menu::select('*')->whereNull('deleted_at')->get();
        return response()->json(
            [
                "status" => "ok",
                "data" => $data
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //cek izin (fitur) access, kalau nggak punya, return json
        // if (!Satpam::cekIzin('create', $this->link_menu)) return response()->json(['status' => 'error', 'message' => 'Tidak diizinkan.']);

        //validasi.
        $validator = \Validator::make($request->all(), [
            'nama_menu' => 'required',
            'menu_parent' => 'required',
            'link_menu' => 'required',
        ]);

        //cek validasi.
        if (!$validator->fails()) {
            // validasi sukses
            $menu_parent = $request->menu_parent;
            if ($menu_parent == null || $menu_parent == '') {
                $menu_parent = 0;
            }
            $menu = new Menu;
            $menu->menu_parent = $menu_parent;
            $menu->nama_menu = $request->nama_menu;
            $menu->link_menu = $request->link_menu;
            $menu->icon = $request->icon;
            $menu->isdelete = false;
            if (is_array($request->fitur_menu)) {
                $menu->fitur_menu = implode("|", $request->fitur_menu);
            } else {
                $menu->fitur_menu = null;
            }

            if (is_array($request->pengunci)) {
                $menu->pengunci = implode("|", $request->pengunci);
            } else {
                $menu->pengunci = null;
            }


            if ($menu->save()) {
                return response()->json(['status' => 'ok']);
            } else {
                return response()->json(['status' => 'error', 'message' => 'Gagal menyimpan data.']);
            }

        }else{
            //validasi error
            return response()->json(['status' => 'validation_error','errors' => $validator->errors()]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Master\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // if (!Satpam::cekIzin('update', $this->link_menu)) return response()->json(['status' => 'error', 'message' => 'Tidak diizinkan.']);

        $data = Menu::where('id', $id)->first();
        return response()->json([
            "status" => 'ok',
            "data" => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Master\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // if (!Satpam::cekIzin('update', $this->link_menu)) return response()->json(['status' => 'error', 'message' => 'Tidak diizinkan.']);

        //validasi.
        $validator = \Validator::make($request->all(), [
            'nama_menu' => 'required',
            'menu_parent' => 'required',
            'link_menu' => 'required',
        ]);

        //cek validasi.
        if (!$validator->fails()) {
            // validasi sukses
            $menu = Menu::find($id);
            $menu->menu_parent = $request->menu_parent;
            $menu->nama_menu = $request->nama_menu;
            $menu->link_menu = $request->link_menu;
            $menu->icon = $request->icon;
            if (is_array($request->fitur_menu)) {
                $menu->fitur_menu = implode("|", $request->fitur_menu);
            } else {
                $menu->fitur_menu = '';
            }

            if (is_array($request->pengunci)) {
                $menu->pengunci = implode("|", $request->pengunci);
            } else {
                $menu->pengunci = '';
            }

            if ($menu->save()) {
                return response()->json(['status' => 'ok']);
            } else {
                return response()->json(['status' => 'error', 'message' => 'Gagal menyimpan data.']);
            }
        } else {
            //validasi error
            return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Master\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->id;
        // if (!Satpam::cekIzin('delete', $this->link_menu)) return response()->json(['status' => 'error', 'message' => 'Tidak diizinkan.']);
        for ($i=0; $i < count($id); $i++) { 
            $data = Menu::find($id[$i]['id']);
            $data->deleted_at = date("Y-m-d H:i:s");
            $data->save();
        }
        return response()->json(['status' => "ok"]);
    }
}
