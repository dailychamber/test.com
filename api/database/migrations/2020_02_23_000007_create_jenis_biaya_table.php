<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJenisBiayaTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'jenis_biaya';

    /**
     * Run the migrations.
     * @table jenis_biaya
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('jenis_biaya_id');
            $table->string('jenis_biaya_name', 200);
            $table->string('jenis_biaya_besar', 50);
            $table->string('thn_ajaran_id', 50);
            $table->integer('order_id');
            $table->string('status', 1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
