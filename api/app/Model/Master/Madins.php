<?php

namespace App\model\master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Madins extends Model
{
    protected $table = 'madin';
    protected $primaryKey = 'madin_id';
    public $timestamps = false;
    protected $guarded = ['madin_id'];
    use SoftDeletes;
}
