<?php

namespace App\model\master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kegiatans extends Model
{
    protected $table = 'kegiatan';
    protected $primaryKey = 'kegiatan_id';
    public $timestamps = false;
    protected $guarded = ['kegiatan_id'];
    use SoftDeletes;
}
