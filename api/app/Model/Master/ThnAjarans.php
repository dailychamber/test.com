<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class ThnAjarans extends Model
{
    protected $table = 'thn_ajaran';
    protected $primaryKey = 'thn_ajaran_id';
    public $timestamps = false;
}
