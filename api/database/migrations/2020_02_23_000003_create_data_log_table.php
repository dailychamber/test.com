<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataLogTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'data_log';

    /**
     * Run the migrations.
     * @table data_log
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->string('log_id', 50);
            $table->dateTime('action_date');
            $table->string('user_id', 50);
            $table->string('module', 50);
            $table->text('action');
            $table->text('page_url');
            $table->text('log_key');
            $table->text('log_content');
            $table->string('ip_address', 50);
            $table->text('user_agent');
            $table->text('request_variable');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
