<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJenisBiayaLogKetTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'jenis_biaya_log_ket';

    /**
     * Run the migrations.
     * @table jenis_biaya_log_ket
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('data_id');
            $table->string('SANTRI_ID', 50);
            $table->string('bayar', 50);
            $table->string('thn_ajaran_id', 50);
            $table->string('asrama', 50);
            $table->string('semester', 50);
            $table->string('value', 100);
            $table->string('keterangan', 50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
