<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMadinTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'madin';

    /**
     * Run the migrations.
     * @table madin
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('madin_id');
            $table->string('madin_name', 100);
            $table->integer('order_id');
            $table->integer('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
