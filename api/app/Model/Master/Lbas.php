<?php

namespace App\model\master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lbas extends Model
{
    protected $table = 'lba';
    protected $primaryKey = 'lba_id';
    public $timestamps = false;
    protected $guarded = ['lba_id'];
    use SoftDeletes;
}
