<?php

namespace App\Http\Controllers\api\master;

use App\Http\Controllers\Controller;
use App\model\master\SantriAsramas;
use Illuminate\Http\Request;

class SantriAsramaController extends Controller
{

    public function index()
    {
        $data = SantriAsramas::all();
        return response()->json([
            "status" => 'ok',
            "data" => $data
        ]);
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'asrama_name' => 'required',
            'kategori' => 'required',
            'status' => 'required'
        ]);

        if (!$validator->fails()) {
            // validasi sukses
            $cek = SantriAsramas::where(['asrama_name' => $request->asrama_name, 'kategori' => $request->kategori])->count();
            if ($cek == 0) {
                $santri_asrma = SantriAsramas::create($request->all());
                if ($santri_asrma) {
                    return response()->json(['status' => 'ok', 'message' => 'Data sudah tersimpan']);
                } else {
                    return response()->json(['status' => 'gagal', 'message' => 'Gagal menyimpan data.']);
                }
            } else {
                return response()->json(['status' => 'gagal', 'message' => 'Data sudah digunakan, harap isikan data yang lain.']);
            }
        } else {
            //validasi error
            return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()]);
        }
    }

    public function show($id)
    {
        $data = SantriAsramas::where('asrama_id', $id)->first();
        return response()->json([
            "status" => "ok",
            "data" => $data
        ]);
    }

    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'asrama_name' => 'required',
            'kategori' => 'required',
            'status' => 'required'
        ]);

        //cek validasi.
        if (!$validator->fails()) {
            $santri_asrama = SantriAsramas::where('asrama_id', $id)
                ->update([
                    'asrama_name' => $request->asrama_name,
                    'kategori' => $request->kategori,
                    'status' => $request->status
                ]);

            if ($santri_asrama) {
                return response()->json(['status' => 'ok', 'message' => 'Data berhasil diperbarui']);
            } else {
                return response()->json(['status' => 'gagal', 'message' => 'Gagal menyimpan data.']);
            }
        } else {
            //validasi error
            return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()]);
        }
    }

    public function destroy($id)
    {
        $santri_asrama = SantriAsramas::destroy($id);
        if ($santri_asrama) {
            return response()->json(['status' => 'Data berhasil dihapus']);
        } else {
            return response()->json(['status' => 'gagal', 'message' => 'Gagal menghapus data.']);
        }
    }
}
