<?php

namespace App\Custom;

use App\Model\Setting\Menu;
use Illuminate\Http\Request;

class SideMenu
{ 
    function render($active=""){
        //shared view data untuk menu
        $sideMenu = Menu::where('menu_parent', 0)->get();
        return $this->tataMenu($sideMenu, $active);
    }

    private function tataMenu($menu, $active, $child = false)
    {
        $html = "";
        if ($child) {
            $html .= "<ul>";
        }

        foreach ($menu as $menuItem) {
            if (strpos($active, $menuItem->link_menu) === 0) {
                $html .= "<li class='active'>";
            } else {
                $html .= "<li>";
            }
            $html .= "
            <a href='" . url($menuItem->link_menu) . "' title='" . $menuItem->nama_menu . "' data-filter-tags='" . $menuItem->nama_menu . "'>
                <i class='" . $menuItem->icon . "'></i>
                <span class='nav-link-text' data-i18n='nav." . $menuItem->nama_menu . "'>" . $menuItem->nama_menu . "</span>
            </a>
            ";
            if ($menuItem->tree->count() > 0) {
                $html .= $this->tataMenu($menuItem->tree, $active, true);
            }
            $html .= "</li>";
        }

        if ($child) {
            $html .= "</ul>";
        }
        return $html;
    }
}
