<?php

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menu')->insert([[
            'id'=>'5',
            'menu_parent'=>'0',
            'nama_menu'=>'Beranda',
            'link_menu'=>'beranda',
            'icon'=>'fal fa-home',
            'fitur_menu'=>'read',
            'pengunci'=>'penuh',
            ],[
            'id'=>'7',
            'menu_parent'=>'0',
            'nama_menu'=>'Input Data',
            'link_menu'=>'input',
            'icon'=>'fal fa-plus-circle',
            'fitur_menu'=>'create|read',
            'pengunci'=>'penuh',
            ],[
            'id'=>'8',
            'menu_parent'=>'0',
            'nama_menu'=>'Approval Atasan',
            'link_menu'=>'#',
            'icon'=>'fal fa-check-circle',
            'fitur_menu'=>'read|update',
            'pengunci'=>'penuh',
            ],[
            'id'=>'9',
            'menu_parent'=>'0',
            'nama_menu'=>'Status Aktifitas1',
            'link_menu'=>'status/proses',
            'icon'=>'fal fa-archive',
            'fitur_menu'=>'read|update|export',
            'pengunci'=>'penuh',
            ],[
            'id'=>'10',
            'menu_parent'=>'0',
            'nama_menu'=>'Setting',
            'link_menu'=>'#',
            'icon'=>'fal fa-cog',
            'fitur_menu'=>'',
            'pengunci'=>'',
            ],[
            'id'=>'11',
            'menu_parent'=>'10',
            'nama_menu'=>'Menu',
            'link_menu'=>'setting/menu',
            'icon'=>'',
            'fitur_menu'=>'create|read|update|delete',
            'pengunci'=>'penuh',
            ],[
            'id'=>'12',
            'menu_parent'=>'10',
            'nama_menu'=>'Role',
            'link_menu'=>'role',
            'icon'=>'',
            'fitur_menu'=>'create|read|update|delete',
            'pengunci'=>'penuh',
            ],[
            'id'=>'13',
            'menu_parent'=>'10',
            'nama_menu'=>'Permission',
            'link_menu'=>'permission',
            'icon'=>'',
            'fitur_menu'=>'read|update',
            'pengunci'=>'penuh',
            ],[
            'id'=>'14',
            'menu_parent'=>'10',
            'nama_menu'=>'Pengguna Aplikasi',
            'link_menu'=>'user',
            'icon'=>'',
            'fitur_menu'=>'create|read|update|delete',
            'pengunci'=>'penuh',
            ],[
            'id'=>'17',
            'menu_parent'=>'10',
            'nama_menu'=>'Lokasi Absen',
            'link_menu'=>'listlokasi',
            'icon'=>'',
            'fitur_menu'=>'create|read|update|delete',
            'pengunci'=>'penuh',
            ],[
            'id'=>'18',
            'menu_parent'=>'0',
            'nama_menu'=>'Live',
            'link_menu'=>'#',
            'icon'=>'fal fa-users',
            'fitur_menu'=>'create|read|update|delete',
            'pengunci'=>'penuh',
            ],[
            'id'=>'19',
            'menu_parent'=>'18',
            'nama_menu'=>'Users',
            'link_menu'=>'live',
            'icon'=>'',
            'fitur_menu'=>'create|read|update|delete',
            'pengunci'=>'penuh',
            ],[
            'id'=>'23',
            'menu_parent'=>'0',
            'nama_menu'=>'test',
            'link_menu'=>'test',
            'icon'=>'',
            'fitur_menu'=>'create',
            'pengunci'=>'opd',
            ]]);
    }
}
