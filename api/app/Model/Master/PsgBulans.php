<?php

namespace App\model\master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PsgBulans extends Model
{
    protected $table = 'psg_bulan';
    protected $primaryKey = 'PSG_ID';
    public $timestamps = false;
    protected $guarded = ['PSG_ID'];
    use SoftDeletes;
}
