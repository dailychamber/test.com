<?php

namespace App\model\master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MadinKelas extends Model
{
    protected $table = 'madin_kelas';
    protected $primaryKey = 'madin_kelas_id';
    public $timestamps = false;
    protected $guarded = ['madin_kelas_id'];
    use SoftDeletes;
}
