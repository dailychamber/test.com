<?php

namespace App\model\master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Formalkelass extends Model
{
    protected $table = 'formal_kelas';
    protected $primaryKey = 'formal_kelas_id';
    public $timestamps = false;
    protected $guarded = ['formal_kelas_id'];
    use SoftDeletes;
}
