<?php

namespace App\Http\Controllers\api\master;

use App\Http\Controllers\Controller;
use App\model\master\PbkGroups;
use Illuminate\Http\Request;

class PbkGroupController extends Controller
{
    public function index()
    {
        $data = PbkGroups::all();
        return response()->json([
            "status" => 'ok',
            "data" => $data
        ]);
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required',
            'group_code' => 'required',
            'kategory' => 'required',
            'status' => 'required'
        ]);

        if (!$validator->fails()) {
            // validasi sukses
            $cek = PbkGroups::where(['name' => $request->name, 'group_code' => $request->group_code])->count();
            if ($cek == 0) {
                $pbk_group = PbkGroups::create($request->all());
                if ($pbk_group) {
                    return response()->json(['status' => 'ok', 'message' => 'Data sudah tersimpan']);
                } else {
                    return response()->json(['status' => 'gagal', 'message' => 'Gagal menyimpan data.']);
                }
            } else {
                return response()->json(['status' => 'gagal', 'message' => 'Data sudah digunakan, harap isikan data yang lain.']);
            }
        } else {
            //validasi error
            return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()]);
        }
    }

    public function show($id)
    {
        $data = PbkGroups::where('id', $id)->first();
        return response()->json([
            "status" => "ok",
            "data" => $data
        ]);
    }

    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required',
            'group_code' => 'required',
            'kategory' => 'required',
            'status' => 'required'
        ]);
        //cek validasi.
        if (!$validator->fails()) {
            $pbk_group = PbkGroups::where('id', $id)
                ->update([
                    'name' => $request->name,
                    'group_code' => $request->group_code,
                    'kategory' => $request->kategory,
                    'status' => $request->status
                ]);

            if ($pbk_group) {
                return response()->json(['status' => 'ok', 'message' => 'Data berhasil diperbarui']);
            } else {
                return response()->json(['status' => 'gagal', 'message' => 'Gagal menyimpan data.']);
            }
        } else {
            //validasi error
            return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()]);
        }
    }

    public function destroy($id)
    {
        $pbk_group = PbkGroups::destroy($id);
        if ($pbk_group) {
            return response()->json(['status' => 'Data berhasil dihapus']);
        } else {
            return response()->json(['status' => 'gagal', 'message' => 'Gagal menghapus data.']);
        }
    }
}
