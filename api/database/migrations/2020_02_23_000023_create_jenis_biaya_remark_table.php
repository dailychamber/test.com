<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJenisBiayaRemarkTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'jenis_biaya_remark';

    /**
     * Run the migrations.
     * @table jenis_biaya_remark
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('data_id');
            $table->string('SANTRI_ID', 50);
            $table->string('thn_ajaran_id', 50);
            $table->string('uang_kembali', 50);
            $table->text('keterangan');
            $table->dateTime('created_at');
            $table->string('created_by', 50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
