<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class UnitFormal extends Model
{
    protected $table = 'unit_formal';
    protected $primaryKey = 'id';
}
