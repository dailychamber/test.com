<?php

use Illuminate\Database\Seeder;

class ProfileTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profile')->insert([[
            'id'=>'1',
            'noktp' => '123456678967',
            'nama_lengkap'=>'admin',
            'alamat'=>'Encrypted Address',
            'nohp'=>'083453563335',
            'nohpkuasa'=>'083453563335',
            'foto'=>'083453563335.jpg'
        ]]);
    }
}
