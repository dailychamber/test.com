<?php

namespace App\model\master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DatiSatu extends Model
{
    protected $table = 'dati_satu';
    protected $primaryKey = 'datu_satu_id';
    public $timestamps = false;
    protected $guarded = ['datu_satu_id'];
    use SoftDeletes;
}
