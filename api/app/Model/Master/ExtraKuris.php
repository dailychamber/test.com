<?php

namespace App\Model\master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExtraKuris extends Model
{
    protected $table = 'extra_kuri';
    protected $primaryKey = 'extra_kur_id';
    public $timestamps = false;
    protected $fillable = ['extra_kur_name', 'status'];
    use SoftDeletes;
}
