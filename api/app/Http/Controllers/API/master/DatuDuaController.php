<?php

namespace App\Http\Controllers\api\master;

use App\Http\Controllers\Controller;
use App\model\master\DatiDua;
use Illuminate\Http\Request;

class DatiDuaController extends Controller
{
    public function index()
    {
        $data = DatiDua::all();
        return response()->json([
            "status" => 'ok',
            "data" => $data
        ]);
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'dati_dua_name' => 'required',
            'dati_satu_id' => 'required',
            'uniq_color' => 'required',
            'status' => 'required'
        ]);

        if (!$validator->fails()) {
            // validasi sukses
            $cek = DatiDua::where(['dati_satu_id' => $request->dati_satu_id])->count();
            if ($cek == 0) {
                $dati_dua = DatiDua::create($request->all());
                if ($dati_dua) {
                    return response()->json(['status' => 'ok', 'message' => 'Data sudah tersimpan']);
                } else {
                    return response()->json(['status' => 'gagal', 'message' => 'Gagal menyimpan data.']);
                }
            } else {
                return response()->json(['status' => 'gagal', 'message' => 'Data sudah digunakan, harap isikan data yang lain.']);
            }
        } else {
            //validasi error
            return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()]);
        }
    }

    public function show($id)
    {
        $data = DatiDua::where('dati_dua_id', $id)->first();
        return response()->json([
            "status" => "ok",
            "data" => $data
        ]);
    }

    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'dati_dua_name' => 'required',
            'dati_satu_id' => 'required',
            'uniq_color' => 'required',
            'status' => 'required'
        ]);

        //cek validasi.
        if (!$validator->fails()) {
            $dati_dua = DatiDua::where('dati_dua_id', $id)
                ->update([
                    'dati_dua_name' => $request->dati_dua_name,
                    'dati_satu_id' => $request->dati_dua_name,
                    'uniq_color' => $request->uniq_color,
                    'status' => $request->status
                ]);

            if ($dati_dua) {
                return response()->json(['status' => 'ok', 'message' => 'Data berhasil diperbarui']);
            } else {
                return response()->json(['status' => 'gagal', 'message' => 'Gagal menyimpan data.']);
            }
        } else {
            //validasi error
            return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()]);
        }
    }

    public function destroy($id)
    {
        $dati_dua = DatiDua::destroy($id);
        if ($dati_dua) {
            return response()->json(['status' => 'Data berhasil dihapus']);
        } else {
            return response()->json(['status' => 'gagal', 'message' => 'Gagal menghapus data.']);
        }
    }
}
