<?php

namespace App\Http\Controllers\api\master;

use App\Http\Controllers\Controller;
use App\model\master\PsgBulans;
use Illuminate\Http\Request;

class PsgBulanController extends Controller
{
    public function index()
    {
        $data = PsgBulans::all();
        return response()->json([
            "status" => 'ok',
            "data" => $data
        ]);
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'santri_id' => 'required',
            'santri_asrama' => 'required',
            'kamar_id' => 'required',
            'thn_ajaran_id' => 'required',
            'date_from' => 'required',
            'date_to' => 'required',
            'bulan' => 'required',
            'tahun' => 'required',
            'status' => 'required'
        ]);

        if (!$validator->fails()) {
            // validasi sukses
            $cek = PsgBulans::where(['santri_id' => $request->santri_id, 'thn_ajaran_id' => $request->thn_ajaran_id])->count();
            if ($cek == 0) {
                $psg_bulan = psg_bulanBulans::create($request->all());
                if ($psg_bulan) {
                    return response()->json(['status' => 'ok', 'message' => 'Data sudah tersimpan']);
                } else {
                    return response()->json(['status' => 'gagal', 'message' => 'Gagal menyimpan data.']);
                }
            } else {
                return response()->json(['status' => 'gagal', 'message' => 'Data sudah digunakan, harap isikan data yang lain.']);
            }
        } else {
            //validasi error
            return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()]);
        }
    }

    public function show($id)
    {
        $data = PsgBulans::where('psg_id', $id)->first();
        return response()->json([
            "status" => "ok",
            "data" => $data
        ]);
    }

    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'santri_id' => 'required',
            'santri_asrama' => 'required',
            'kamar_id' => 'required',
            'thn_ajaran_id' => 'required',
            'date_from' => 'required',
            'date_to' => 'required',
            'bulan' => 'required',
            'tahun' => 'required',
            'status' => 'required'
        ]);

        //cek validasi.
        if (!$validator->fails()) {
            $psg_bulan = PsgBulans::where('psg_id', $id)
                ->update([
                    'santri_id' => $request->santri_id,
                    'santri_asrama' => $request->santri_asrama,
                    'kamar_id' => $request->kamar_id,
                    'thn_ajaran_id' => $request->thn_ajaran_id,
                    'date_from' => $request->date_from,
                    'date_to' => $request->date_to,
                    'bulan' => $request->bulan,
                    'tahun' => $request->tahun,
                    'status' => $request->status
                ]);

            if ($psg_bulan) {
                return response()->json(['status' => 'ok', 'message' => 'Data berhasil diperbarui']);
            } else {
                return response()->json(['status' => 'gagal', 'message' => 'Gagal menyimpan data.']);
            }
        } else {
            //validasi error
            return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()]);
        }
    }

    public function destroy($id)
    {
        $psg_bulan = PsgBulans::destroy($id);
        if ($psg_bulan) {
            return response()->json(['status' => 'Data berhasil dihapus']);
        } else {
            return response()->json(['status' => 'gagal', 'message' => 'Gagal menghapus data.']);
        }
    }
}
