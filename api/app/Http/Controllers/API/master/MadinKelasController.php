<?php

namespace App\Http\Controllers\api\master;

use App\Http\Controllers\Controller;
use App\model\master\MadinKelas;
use Illuminate\Http\Request;

class MadinKelasController extends Controller
{
    public function index()
    {
        $data = MadinKelas::all();
        return response()->json([
            "status" => 'ok',
            "data" => $data
        ]);
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'madin_id' => 'required',
            'madin_kelas_name' => 'required',
            'order_id' => 'required',
            'status' => 'required'
        ]);

        if (!$validator->fails()) {
            // validasi sukses
            $cek = MadinKelas::where(['madin_id' => $request->madin_id])->count();
            if ($cek == 0) {
                $madin_kelas = MadinKelas::create($request->all());
                if ($madin_kelas) {
                    return response()->json(['status' => 'ok', 'message' => 'Data sudah tersimpan']);
                } else {
                    return response()->json(['status' => 'gagal', 'message' => 'Gagal menyimpan data.']);
                }
            } else {
                return response()->json(['status' => 'gagal', 'message' => 'Data sudah digunakan, harap isikan data yang lain.']);
            }
        } else {
            //validasi error
            return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()]);
        }
    }

    public function show($id)
    {
        $data = MadinKelas::where('madin_kelas_id', $id)->first();
        return response()->json([
            "status" => "ok",
            "data" => $data
        ]);
    }

    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'madin_id' => 'required',
            'madin_kelas_name' => 'required',
            'order_id' => 'required',
            'status' => 'required'
        ]);
        //cek validasi.
        if (!$validator->fails()) {
            $madin_kelas = MadinKelas::where('madin_kelas_id', $id)
                ->update([
                    'madin_id' => $request->madin_id,
                    'madin_kelas_name' => $request->madin_kelas_name,
                    'order_id' => $request->order_id,
                    'status' => $request->status
                ]);

            if ($madin_kelas) {
                return response()->json(['status' => 'ok', 'message' => 'Data berhasil diperbarui']);
            } else {
                return response()->json(['status' => 'gagal', 'message' => 'Gagal menyimpan data.']);
            }
        } else {
            //validasi error
            return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()]);
        }
    }

    public function destroy($id)
    {
        $madin_kelas = MadinKelas::destroy($id);
        if ($madin_kelas) {
            return response()->json(['status' => 'Data berhasil dihapus']);
        } else {
            return response()->json(['status' => 'gagal', 'message' => 'Gagal menghapus data.']);
        }
    }
}
