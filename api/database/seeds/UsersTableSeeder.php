<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([[
            'id'=>'1',
            'profile_id'=>'1',
            'name'=>'admin',
            'email'=>'admin@pasuruankab.go.id',
            'password'=>Hash::make('enakmoro')
        ]]);
    }
}
