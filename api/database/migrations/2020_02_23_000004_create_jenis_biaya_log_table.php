<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJenisBiayaLogTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'jenis_biaya_log';

    /**
     * Run the migrations.
     * @table jenis_biaya_log
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('data_id');
            $table->string('jenis_biaya_id', 50);
            $table->string('SANTRI_ID', 50);
            $table->integer('bulan');
            $table->string('tahun', 50);
            $table->string('bayar', 50);
            $table->string('thn_ajaran_id', 50);
            $table->string('santri_kamar_id', 50);
            $table->string('asrama', 50);
            $table->dateTime('created_at');
            $table->string('created_by', 50);
            $table->string('status', 1);
            $table->string('uniq_code', 50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
