<?php

namespace App\Http\Controllers\API\master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Master\UnitFormal;

class UnitFormalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = UnitFormal::whereNull('deleted_at')->get();
        return response()->json([
            "status" => 'ok',
            "data" => $data
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'nama' => 'required'
        ]);

        //cek validasi.
        if (!$validator->fails()) {
            // validasi sukses
            $cek = UnitFormal::where(['nama' => $request->nama])->count();
            if ($cek == 0) {
                $unit_formal = new UnitFormal;
                $unit_formal->nama = $request->nama;
                $unit_formal->status = 'Aktif';
                if ($unit_formal->save()) {
                    return response()->json(['status' => 'ok']);
                } else {
                    return response()->json(['status' => 'gagal', 'message' => 'Gagal menyimpan data.']);
                }
            } else {
                return response()->json(['status' => 'gagal', 'message' => 'Data sudah digunakan, harap isikan data yang lain.']);
            }
        } else {
            //validasi error
            return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = UnitFormal::where('id', $id)->first();
        return response()->json([
            "status" => "ok",
            "data" => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'nama' => 'required'
        ]);

        //cek validasi.
        if (!$validator->fails()) {
            // validasi sukses
<<<<<<< HEAD
=======
<<<<<<< HEAD
            $unit_formal = UnitFormal::find($id);
            $unit_formal->nama = $request->nama;
            $unit_formal->status = $request->status;
            if ($unit_formal->save()) {
                return response()->json(['status' => 'ok']);
            } else {
                return response()->json(['status' => 'gagal', 'message' => 'Gagal menyimpan data.']);
            }
=======
>>>>>>> update api
                $unit_formal = UnitFormal::find($id);
                $unit_formal->nama = $request->nama;
                $unit_formal->status = $request->status;
                if ($unit_formal->save()) {
                    return response()->json(['status' => 'ok']);
                } else {
                    return response()->json(['status' => 'gagal', 'message' => 'Gagal menyimpan data.']);
                }
<<<<<<< HEAD
=======
>>>>>>> b54086b7f0c74245440c185c58358afcd951e2cf
>>>>>>> update api
        } else {
            //validasi error
            return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()]);
        }
    }

<<<<<<< HEAD
    /**
=======
<<<<<<< HEAD
    /** 
=======
    /**
>>>>>>> b54086b7f0c74245440c185c58358afcd951e2cf
>>>>>>> update api
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $unit_formal = UnitFormal::find($id);
        $unit_formal->deleted_at = date('Y-m-d H:i:s');
        if ($unit_formal->save()) {
            return response()->json(['status' => 'ok']);
        } else {
            return response()->json(['status' => 'gagal', 'message' => 'Gagal menghapus data.']);
        }
    }
}
