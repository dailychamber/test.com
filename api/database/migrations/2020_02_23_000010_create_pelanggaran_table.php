<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePelanggaranTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'pelanggaran';

    /**
     * Run the migrations.
     * @table pelanggaran
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('data_id');
            $table->date('data_date');
            $table->string('hari', 1);
            $table->string('SANTRI_ID', 50);
            $table->text('pelanggaran');
            $table->text('saksi');
            $table->text('proses');
            $table->string('thn_ajaran_id', 50);
            $table->string('created_by', 50);
            $table->string('updated_by', 50);
            $table->string('asrama_id', 50);
            $table->text('asrama');
            $table->string('kamar_id', 50);
            $table->text('kamar');
            $table->text('lembaga');
            $table->text('alamat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
