<?php

namespace App\Model\Setting;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Menu extends Model
{
    protected $table = 'menu';
    protected $fillable = [
        'menu_parent', 'nama_menu', 'link_menu', 'icon', 'fitur_menu', 'pengunci'
    ];
    protected $with = [
        'tree'
    ];
    

    public function tree(){
        return $this->hasMany(Menu::class,'menu_parent','id');
    }

    /**
     * datatable
     *
     * @return void
     */
    static function datatable(){
        return DB::table('menu as a')
            ->select("a.id", "a.nama_menu", "b.nama_menu as nama_parent", "a.link_menu","a.fitur_menu","a.pengunci")
            ->leftJoin('menu as b', 'a.menu_parent', '=', 'b.id');
    }

    static function getSidebar(){
    }
}
